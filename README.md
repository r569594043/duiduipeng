# 对对碰前端代码

## 目录结构
    root ------------------ 根目录(html以及gulp构建相关文件)
    ├─css ----------------- 样式表文件
    │  └─libs ------------- 第三方样式(同less目录中的libs)
    │      └─jquery-ui ---- jQuery UI样式表
    ├─img ----------------- 图片
    │  └─jquery-ui -------- jQuery UI图片
    ├─js ------------------ 脚本文件
    │  └─libs ------------- 第三方脚本文件(jQuery等)
    │      └─jquery-ui ---- jQuery UI
    └─less ---------------- LESS文件
        └─libs ------------ 第三方样式


## 文件

### root

* README.md --------------- 本文件
* .gitignore -------------- Git 忽略文件
* gulpfile.js ------------- gulp 构建
* package.json ------------ npm
* index.html -------------- 个人主页
* popup.html -------------- 弹出框(仅开发的时候看效果用，代码已拷贝到具体页面)
* same-taste.html --------- 同味人页面
* publish-date.html ------- 发布约会页面
* choose-restaurant.html -- 选择餐厅页面
* restaurant-detail.html -- 餐厅详情页面
* duiduipeng.html --------- 缘分对对碰页面
* send-invitation.html ---- 吃饭邀约页面
* send-success.html ------- 吃饭邀约-发送成功页面
* other-profile.html ------ Ta的主页
* signin.html ------------- 注册登录页面



### less

* prefixer.less ----------- CSS 3前缀mixins
* mixins.less ------------- 常用mixins
* variable.less ----------- 公用的LESS变量
* common.less ------------- 公用的样式
* index.less -------------- 个人主页样式
* popup.less -------------- 弹出框样式
* same-taste.less --------- 同味人样式
* publish-date.less ------- 发布约会样式
* choose-restaurant.less -- 选择餐厅样式
* restaurant-detail.less -- 餐厅详情样式
* duiduipeng.less --------- 缘分对对碰样式
* send-invitation.less ---- 吃饭邀约样式
* send-success.less ------- 吃饭邀约-发送成功样式
* other-profile.less ------ Ta的主页样式
* signin.less ------------- 注册登录样式
* libs/normalize.css - CSS resets. [Normalize.css](http://necolas.github.io/normalize.css/)  
附：LESS [官网(需翻墙)](http://lesscss.org/) [中文文档1](http://lesscss.net/) [中文文档2](http://www.bootcss.com/p/lesscss/)  
注：`prefixer.less` `mixins.less` `variable.less` 仅作为其他less文件引用，生成的css文件为空，可**直接删除**


### css

less编译生成的css文件  
具体用途参见[less](#less)文件注释  
注：`prefixer.css` `mixins.css` `variable.css` 三个文件无用，为空文件，可以**直接删除**  


### js

* common.js --------------------------- 公用的js
* index.js ---------------------------- 个人主页js
* popup.js ---------------------------- 弹框js
* publish-date.js --------------------- 发布约会js
* choose-restaurant.js ---------------- 选择餐厅js
* duiduipeng.js ----------------------- 缘分对对碰js
* send-invitation.js ------------------ 吃饭邀约js
* other-profile.js -------------------- Ta的主页js
* signin.js --------------------------- 注册登录js
* libs/html5shiv.js ------------------- [html5 shiv](https://github.com/aFarkas/html5shiv)
* libs/jquery-1.11.2.js --------------- [jQuery](http://jquery.com/)
* libs/jquery.cookie.js --------------- [jQuery cookie插件](https://github.com/carhartl/jquery-cookie)
* libs/jquery.bpopup.js --------------- [jQuery bPopup弹框插件](http://www.dinbror.dk/bpopup/)
* libs/jquery.validate.js ------------- [jQuery 表单验证插件](http://jqueryvalidation.org/)
* libs/ajaxfileupload.js -------------- [jQuery Ajax文件上传插件](http://www.phpletter.com/demo/ajaxfileupload-demo/)
* libs/jQueryRotate.js ---------------- [jQuery 旋转插件(需翻墙)](https://code.google.com/p/jquery-rotate/)
* libs/json2.js ----------------------- [json2](https://github.com/douglascrockford/JSON-js)
* libs/jquery-ui/jquery-ui.js --------- [jQuery UI](http://jqueryui.com/)
* libs/jquery-ui/datepicker-zh-CN.js -- [jQuery UI日历控件汉化文件](https://github.com/jquery/jquery-ui/tree/master/ui/i18n)

 

### img

略

## gulp构建
###安装依赖
打开命令行并进入到项目目录，运行如下命令：

    npm install

###开发时
打开命令行并进入到项目目录，运行如下命令：

    gulp

当看到如下字样时：

    $[10:30:04] Using gulpfile \your project directory\gulpfile.js
    $[10:30:04] Starting 'watch'...
    $[10:30:04] Finished 'watch' after 25 ms
    $[10:30:04] Starting 'default'...
    $[10:30:04] Finished 'default' after 56 μs

代表gulp启动成功，此时编写less文件会自动编译更新相应css文件

###编译全部less文件
打开命令行并进入到项目目录，运行如下命令：

    gulp compile

###编译全部less文件并生成压缩的css文件
打开命令行并进入到项目目录，运行如下命令：

    gulp build

附：Gulp [官网](http://gulpjs.com/) [文档](https://github.com/gulpjs/gulp/blob/master/docs/API.md)  
