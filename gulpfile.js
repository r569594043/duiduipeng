var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var del = require('del');
var path = require('path');
var util = require('util');
//var spawn = require('child_process').spawn;


function now() {
    var d = new Date();
    var hour = d.getHours();
    hour = hour < 10 ? "0" + hour : "" + hour;
    var minute = d.getMinutes();
    minute = minute < 10 ? "0" + minute : "" + minute;
    var second = d.getSeconds();
    second = second < 10 ? "0" + second : "" + second;
    return hour + ":" + minute + ":" + second;
}


function color(text, color) {
    var styles = {
        'bold'          : ['\x1B[1m',  '\x1B[22m'],
        'italic'        : ['\x1B[3m',  '\x1B[23m'],
        'underline'     : ['\x1B[4m',  '\x1B[24m'],
        'inverse'       : ['\x1B[7m',  '\x1B[27m'],
        'strikethrough' : ['\x1B[9m',  '\x1B[29m'],
        'white'         : ['\x1B[37m', '\x1B[39m'],
        'grey'          : ['\x1B[90m', '\x1B[39m'],
        'black'         : ['\x1B[30m', '\x1B[39m'],
        'blue'          : ['\x1B[34m', '\x1B[39m'],
        'cyan'          : ['\x1B[36m', '\x1B[39m'],
        'green'         : ['\x1B[32m', '\x1B[39m'],
        'magenta'       : ['\x1B[35m', '\x1B[39m'],
        'red'           : ['\x1B[31m', '\x1B[39m'],
        'yellow'        : ['\x1B[33m', '\x1B[39m'],
        'whiteBG'       : ['\x1B[47m', '\x1B[49m'],
        'greyBG'        : ['\x1B[49;5;8m', '\x1B[49m'],
        'blackBG'       : ['\x1B[40m', '\x1B[49m'],
        'blueBG'        : ['\x1B[44m', '\x1B[49m'],
        'cyanBG'        : ['\x1B[46m', '\x1B[49m'],
        'greenBG'       : ['\x1B[42m', '\x1B[49m'],
        'magentaBG'     : ['\x1B[45m', '\x1B[49m'],
        'redBG'         : ['\x1B[41m', '\x1B[49m'],
        'yellowBG'      : ['\x1B[43m', '\x1B[49m']
    };
    if(styles.hasOwnProperty(color) && styles[color].length) {
        return styles[color][0] + text + styles[color][1];
    }
    return text;
}

// function log(text) {
//     var args = Array.prototype.splice.call(arguments, 1);
//     var text = util.format.apply(null, ["[%s] " + text, color(now(), "grey")].concat(args));
//     console.log(text);
// }

function log() {
    var text = util.format.apply(null, arguments);
    console.log("[%s] %s", color(now(), "grey"), text);
}

gulp.task('watch', function() {
    gulp.watch('less/*.less', function(event) {
        var relativePath = path.relative(process.cwd(), event.path);
        log("File %s was %s, running tasks...", color(relativePath, "magenta"), color(event.type, "cyan"));
        if(event.type === 'added' || event.type === 'renamed' || event.type === 'changed') {
            gulp.src(relativePath)
                .pipe(less())
                .pipe(gulp.dest('css'));
        }
    });
});

gulp.task("clean", function() {
    // del(['css/**/*.css'], function (err, deletedFiles) {
    //     console.log('Files deleted:', deletedFiles.join(', '));
    // });
    del.sync(['css/**/*.css']);
});

gulp.task("copy", function() {
    return gulp.src('less/libs/*.css')
        .pipe(gulp.dest('css/libs'));
});

gulp.task('less', function() {
    return gulp.src('less/*.less')
        .pipe(less())
        .pipe(gulp.dest('css'));
});

gulp.task("copy+minify", function() {
    gulp.src('less/libs/*.css')
        .pipe(minifyCSS())
        .pipe(gulp.dest('css/libs'));
});

gulp.task("less+minify", function() {
    gulp.src('less/*.less')
        .pipe(less())
        .pipe(gulp.dest('css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('css'));
});

gulp.task('build', ["clean", "copy+minify", "less+minify"], function() {
    log("Build done!");
});


gulp.task('compile', ["clean", "copy", "less"], function() {
    log("Compile done!");
});


// gulp.task('auto-reload', function() {
//     var process;

//     function restart() {
//         if (process) {
//             console.log("[%s] Config file changes, " + color("restart", "magenta") + " gulp!", color(now(), "grey"));
//             process.kill();
//         }
//         process = spawn('gulp', ['default'], {stdio: 'inherit'});
//     }

//     gulp.watch('gulpfile.js', restart);
//     restart();
// });

gulp.task('default', ['watch']);