var colors = ["#cc6699", "#ec6253", "#666666", "#998aca", "#4cb9dd", "#996666", "#99cccc", "#ff9933", "#449edd", "#99cc00"];

var hoverColors = ["#bf4080", "#e73825", "#4d4d4d", "#7a67ba", "#27a5cf", "#7a5252", "#77bbbb", "#ff8000", "#2586c9", "#739900"];
$(".tags a").each(function(i, e) {
    var $e = $(e);
    var rand = Math.round(Math.random() * 9);
    var color = colors[rand];
    var hoverColor = hoverColors[rand];
    $e.css("background-color", color);
    $e.on("mouseenter", function() {
        $e.css("background-color", hoverColor);
    });
    $e.on("mouseleave", function() {
        $e.css("background-color", color);
    });
});


$(".selecting-box").on("click", ".unselected li", function(e) {
    var $this = $(this);
    var $tags = $this.closest(".tags");
    var $selectingBox = $tags.closest(".selecting-box");
    var $selectedBox = $selectingBox.prev(".selected-box");
    var $selected = $tags.find(".selected");
    $this.appendTo($selected);
    $(".tags .selected", $selectedBox).append($this.clone());
});

$(".selecting-box").on("click", ".selected li a i", function(e) {
    var $li = $(this).closest("li");
    var $tags = $li.closest(".tags");
    var $selectingBox = $tags.closest(".selecting-box");
    var $selectedBox = $selectingBox.prev(".selected-box");
    var $unSelected = $tags.find(".unselected");
    $li.appendTo($unSelected);
    var tagId = $li.attr("tag-id");
    $(".tags .selected li[tag-id='" + tagId + "']", $selectedBox).remove();
});

$(".selecting-box").on("click", ".btn", function(e) {
    var $this = $(this);
    var $selectingBox = $this.closest(".selecting-box");
    if($(".selected li", $selectingBox).length) {
        $selectingBox.hide().siblings(".selected-box").show();
    } else {
        $selectingBox.hide().siblings(".unselect-box").show();
    }
});

$(".unselect-box").on("click", ".btn", function() {
    var $this = $(this);
    var $unselectBox = $this.closest(".unselect-box");
    $unselectBox.hide().siblings(".selecting-box").show();
});

$(".selected-box").on("click", ".btn", function() {
    var $this = $(this);
    var $selectedBox = $this.closest(".selected-box");
    $selectedBox.hide().siblings(".selecting-box").show();
});


$.datepicker.setDefaults($.datepicker.regional["zh-CN"]);

$("#meal-date").datepicker({

});

$("#meal-date + i").click(function() {
    $("#meal-date").datepicker("show");
});


$("select").change(function(e) {
    $(this).blur();
});

// 以下代码仅供测试看样式效果用
var chooseRestaurant = $.cookie('choose-restaurant');
if(chooseRestaurant) {
    $(".restaurant-choose").hide();
    $(".restaurant-choosen").show();
    $(".restaurant-choosen label").text(chooseRestaurant);
}
// 以上代码仅供测试看样式效果用