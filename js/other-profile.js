// tab切换
$(".cr-left dd").click(function(){
    var active = $(".cr-left dd.active").attr("flag");
    $("div." + active).hide();
    $("div.scan-date").hide();
    var flag = $(this).attr("flag");
    $("div." + flag).show();
    $(".cr-left dd").removeClass("active");
    $(this).addClass("active");
});

var colors = ["#cc6699", "#ec6253", "#666666", "#998aca", "#4cb9dd", "#996666", "#99cccc", "#ff9933", "#449edd", "#99cc00"];
$(".label-group a").each(function(i, e) {
    var rand = Math.round(Math.random() * 9);
    $(this).css("background-color", colors[rand]);
});

$(".location .complete").on("click", function() {
    // bPopup参数参见文档 http://dinbror.dk/bpopup/
    $(".other-info-pop").bPopup({
        "amsl": 0
    });
    return false;
});