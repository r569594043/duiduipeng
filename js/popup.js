;(function(window, undefined) {
    var colors = ["#cc6699", "#ec6253", "#666666", "#998aca", "#4cb9dd", "#996666", "#99cccc", "#ff9933", "#449edd", "#99cc00"];
    $(".user-info-pop .label-group a").each(function(i, e) {
        var rand = Math.round(Math.random() * 9);
        $(this).css("background-color", colors[rand]);
    });
    $(".user-info-pop .label-group a").click(function() {
        $(this).toggleClass("on");
        return false;
    });

    $.datepicker.setDefaults($.datepicker.regional["zh-CN"]);

    $("input#birthday").datepicker({
        maxDate: new Date()
    });

    $(".complete-info-pop .label-group a").each(function(i, e) {
        var rand = Math.round(Math.random() * 9);
        $(this).css("background-color", colors[rand]);
    });

    $(".complete-info-pop .label-group a").click(function() {
        $(this).toggleClass("on");
        return false;
    });

    $(".complete-info-pop .exclusive-labels button").click(function(e) {
        $(".complete-info-pop .exclusive-labels").hide();
        var $selectedTags = $(".complete-info-pop .exclusive-labels .label-group li:has(.on)");
        $(".complete-info-pop .tags .selected").empty().append($selectedTags.clone());
        return false;
    });

    $(".complete-info-pop .tags .selected").on("click", "li a i", function() {
        var $this = $(this);
        var $tag = $this.closest("li");
        var tagId = $tag.attr("tag-id");
        if(tagId) {
            $tag.remove();
            $(".complete-info-pop .exclusive-labels .label-group li[tag-id='" + tagId + "'] a").removeClass("on");
        }
    });


    $("#birthday-cip").datepicker({
        maxDate: new Date()
        // changeMonth: true,
        // changeYear: true
    });

    $(".btn-add-tag").click(function() {
        var $this = $(this);
        $this.next(".exclusive-labels").show();
    });

    function extension(fileName) {
        var splits = fileName.split(".");
        if(splits.length > 0) {
            return splits[splits.length - 1].toLowerCase();
        }
        return "";
    }

    $(".complete-info-pop").on("change", "#upload-photo", function(e) {
        var $this = $(this);
        var filePath = $this.val();
        var ext = extension(filePath);
        if(!(ext === "jpg" || ext === "png" || ext === "jpeg")) {
            alert("头像只支持jpg或png格式的图片");
            return false;
        }
        $.ajaxFileUpload({
            url:'doajaxfileupload.php',
            secureuri:false,
            fileElementId:'upload-photo',
            dataType: 'text',
            success: function (data, status) {
                try {
                    if(data.indexOf("<pre") === 0) {
                        var matches = /<pre.*?>(.*?)<\/pre>/.exec(data);
                        if(matches.length > 1) {
                            data = $.parseJSON(matches[1]);
                        } else {
                            alert("上传失败");
                        }
                    } else if(data.indexOf("{") === 0) {
                        data = $.parseJSON(data);
                    } else {
                        alert("上传失败");
                        return false;
                    }
                    if(data.result) {
                        $("#upload-photo").prev("img").prop("src", data.url).show();
                    } else {
                        if(data.message) {
                            alert(data.message);
                        } else {
                            alert("上传失败");
                        }
                    }
                } catch(e) {
                    alert("上传失败");
                }
            },
            error: function (data, status, e) {
                alert("上传失败");
            }
        });
    });

    $(".complete-info-pop form").on("submit", function() {
        var $form = $(".complete-info-pop form");
        var head = $(".upload img", $form).attr("src");
        if(head === "") {
            alert("请上传头像！");
            return false;
        }
        var nick = $.trim($("#nick", $form).val());
        if(nick === "") {
            alert("请输入昵称！");
            return false;
        }

        var gender = $('[name="gender"]:checked').val();
        if(!gender) {
            alert("请选择性别！");
            return false;
        }


        var birthday = $("#birthday-cip", $form).val();
        if(birthday === "") {
            alert("请选择生日！");
            return false;
        }
        birthday = $("#birthday-cip", $form).datepicker("getDate");
        // 可以进一步判断比如小于10岁大于100岁
        console.log(birthday);
        var income = $("#income", $form).val();
        if(income === "") {
            alert("请选择月薪范围！");
            return false;
        }
        var stature = $("#stature", $form).val();
        if(stature === "") {
            alert("请选择身高！");
            return false;
        }

        var $tags = $(".tags .selected li", $form);
        if($tags.length === 0) {
            alert("请选择标签！");
            return false;
        }
        var $agreeDeal = $("#agree-deal");
        if(!$agreeDeal.is(":checked")) {
            alert("请同意协议！");
            return false;
        }

        // 验证通过
        // 此处实现保存信息的逻辑
        return false;
    });


    var $userInfoPop = $(".user-info-pop");

    var $basicInfoForm = $(".basic-info form", $userInfoPop);

    $basicInfoForm.validate({
        "errorClass": "invalid",
        "rules": {
            "nickname": "required",
            "gender": "required",
            "birthday": "required",
            "stature": {
                "required": true
            },
            "weight": {
                "required": true,
                "pattern": /^[1-9]\d{1,2}$/
            },
            "address": "required",
            "education": "required",
            "school": "required",
            "industry": "required",
            "company": "required",
            "position": "required",
            "income": "required"
        },
        "messages": {
            "nickname": {
                "required": "昵称不能为空"
            },
            "gender": {
                "required": "请选择性别"
            },
            "birthday": {
                "required": "请选择生日"
            },
            "stature": {
                "required": "请选择身高"
            },
            "weight": {
                "required": "请填写体重，单位是公斤",
                "pattern": "体重格式不正确"
            },
            "address": {
                "required": "请填写地址"
            },
            "education": {
                "required": "请选择学历"
            },
            "school": {
                "required": "请填写毕业院校"
            },
            "industry": {
                "required": "请选择行业"
            },
            "company": {
                "required": "请填写工作单位"
            },
            "position": {
                "required": "请填写目前职位"
            },
            "income": {
                "required": "请选择月收入"
            }
        },
        "submitHandler": function(form) {
            // 在此实现保存信息的逻辑，如果通过form表单提交直接将return false 改为 return true;
            alert("验证通过!");
            return false;
        },
        "invalidHandler":function(event, validator) {
            var errorList = validator.errorList;
            if(errorList.length > 0) {
                var first = errorList[0];
                alert(first.message);
                $(first.element).focus();
            }
            return false;
        },
        "errorPlacement": function(error, element) {
            return null;
        }
    });

    var $detailInfoForm = $(".detail-info form", $userInfoPop);

    $detailInfoForm.validate({
        "errorClass": "invalid",
        "rules": {
            "nationality": "required",
            "native-place": "required",
            "constellation": "required",
            "blood-type": {
                "required": true
            },
            "chinese-zodiac": {
                "required": true
            },
            "religious-belief": "required"
        },
        "messages": {
            "nationality": {
                "required": "请选择民族"
            },
            "native-place": {
                "required": "请填写籍贯"
            },
            "constellation": {
                "required": "请选择星座"
            },
            "blood-type": {
                "required": "请选择血型"
            },
            "chinese-zodiac": {
                "required": "请选择属相"
            },
            "religious-belief": {
                "required": "请选择宗教信仰"
            }
        },
        "submitHandler": function(form) {
            // 在此实现保存信息的逻辑，如果通过form表单提交直接将return false 改为 return true;
            alert("验证通过!");
            return false;
        },
        "invalidHandler":function(event, validator) {
            var errorList = validator.errorList;
            if(errorList.length > 0) {
                var first = errorList[0];
                alert(first.message);
                $(first.element).focus();
            }
            return false;
        },
        "errorPlacement": function(error, element) {
            return null;
        }
    });


    $(".exclusive-labels button", $userInfoPop).click(function() {
        var $selectedTags = $(".exclusive-labels .label-group li:has(.on)", $userInfoPop);
        if($selectedTags.length === 0) {
            alert("请选择标签");
            return false;
        }
    });

    $(".inner-chatter button", $userInfoPop).click(function() {
        var innerChatter = $.trim($("#inner-chatter", $userInfoPop).val());
        if(innerChatter === "") {
            alert("内心独白不能为空");
            return false;
        }
        if(innerChatter.length > 300) {
            alert("内心独白的长度不能超过300个汉字");
            return false;
        }

    });

})(window);