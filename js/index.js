// tab切换
$(".cr-left dd").click(function(){
    var active = $(".cr-left dd.active").attr("flag");
    $("div." + active).hide();
    $("div.scan-date").hide();
    var flag = $(this).attr("flag");
    $("div." + flag).show();
    $(".cr-left dd").removeClass("active");
    $(this).addClass("active");
});

$(".cr-right .published-date .view").click(function(){
    $(".cr-right .published-date").hide();
    $(".cr-right .scan-date").show();
    return false;
});

$(".cr-right .scan-date .return-back a").click(function(){
    $(".cr-right .scan-date").hide();
    $(".cr-right .published-date").show();
    return false;
});

$(".content-left ul li").click(function(){
    $(".content-left ul li").removeClass("active");
    if(!$(this).hasClass("pictrue")){
        $(".user-img").hide();
    }else{
        $(".user-img").show();
    }
    $(this).addClass("active");
    return false;
});

// 子tab页切换
$(".tabs li").click(function() {
    var $this = $(this);
    var $tabs = $this.closest(".tabs");
    if($this.is(".active")) {
        return false;
    }
    var index = $("li", $tabs).index($this);

    $this.addClass("active").siblings().removeClass("active");

    var $tabContent = $tabs.next(".tab-content");

    $(".tab-pane", $tabContent).removeClass("active").eq(index).addClass("active");

    return false;
});

$(".location .complete").on("click", function() {
    // bPopup参数参见文档 http://dinbror.dk/bpopup/
    $(".user-info-pop").bPopup({
        "amsl": 0
    });
    return false;
});

$(".tags .add-tag").on("click", function() {
    $(".user-info-pop").bPopup({
        "amsl": 0
    });
    return false;
});

$(".intro .edit").on("click", function() {
    $(".user-info-pop").bPopup({
        "amsl": 0
    });
    return false;
});

$(".info .detail .upgrade").on("click", function() {
    $(".level-up-pop").bPopup({
        "amsl": 0
    });
    return false;
});


// 查看约会消息
$(".date-msg").on("click", "tr:has(a > span)", function() {
    var $this = $(this);
    var $detail = $this.next("tr.detail");
    if($this.is(".opened")) {
        if($detail.is(":visible")) {
            $this.removeClass("opened");
            $detail.hide();

        }
    } else {
        if(!$detail.is(":visible")) {
            $this.addClass("opened");
            if($this.is(".unread")) {
                $this.removeClass("unread");
            }
            $detail.show();
        }
    }
});


$(".system-notice").on("click", "tr.title", function() {
    var $this = $(this);
    var $detail = $this.next("tr.detail");
    if($this.is(".opened")) {
        if($detail.is(":visible")) {
            $this.removeClass("opened");
            $detail.hide();

        }
    } else {
        if(!$detail.is(":visible")) {
            $this.addClass("opened");
            $detail.show();
        }
    }
});