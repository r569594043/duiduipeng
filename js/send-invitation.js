$.datepicker.setDefaults($.datepicker.regional["zh-CN"]);

$("#meal-date").datepicker({

});

$("#meal-date + i").click(function() {
    $("#meal-date").datepicker("show");
});

$("form").on("submit", function() {
    window.location.href = "send-success.html";
    return false;
});

// 以下代码仅供测试看样式效果用
var chooseRestaurant = $.cookie('choose-restaurant');
if(chooseRestaurant) {
    $(".restaurant-choose").hide();
    $(".restaurant-choosen").show();
    $(".restaurant-choosen label").text(chooseRestaurant);
}
// 以上代码仅供测试看样式效果用