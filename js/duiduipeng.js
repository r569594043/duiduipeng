var colors = ["#cc6699", "#ec6253", "#666666", "#998aca", "#4cb9dd", "#996666", "#99cccc", "#ff9933", "#449edd", "#99cc00"];

var hoverColors = ["#bf4080", "#e73825", "#4d4d4d", "#7a67ba", "#27a5cf", "#7a5252", "#77bbbb", "#ff8000", "#2586c9", "#739900"];
$(".tags a").each(function(i, e) {
    var $e = $(e);
    var rand = Math.round(Math.random() * 9);
    var color = colors[rand];
    var hoverColor = hoverColors[rand];
    $e.css("background-color", color);
    $e.on("mouseenter", function() {
        $e.css("background-color", hoverColor);
    });
    $e.on("mouseleave", function() {
        $e.css("background-color", color);
    });
});


$(".selecting-box").on("click", ".unselected li", function(e) {
    var $this = $(this);
    var $unSelected = $this.closest(".unselected").hide();
    var $tags = $this.closest(".tags");
    var $selectingBox = $tags.closest(".selecting-box");
    var $selected = $tags.find(".selected").show();
    $this.clone().appendTo($selected);
});

$(".selecting-box").on("click", ".selected li a i", function(e) {
    var $li = $(this).closest("li");
    var $selected = $li.closest(".selected").hide();
    var $tags = $li.closest(".tags");
    var $selectingBox = $tags.closest(".selecting-box");
    var $unSelected = $tags.find(".unselected").show();
    // $li.appendTo($unSelected);
    $li.remove();
    var tagId = $li.attr("tag-id");
});

var turning = false;
$(".pointer").click(function(){
    if(turning) {
        return false;
    }
    turning = true;
    for(var i = 0; i <= 10000; i++){
        $("#turnplate-img").rotate({
            animateTo: i,
            duration: 3000
        });
        if (i >= 1800){ //设置转盘停位置
            break;
        }
    }
    setTimeout(function() {
        $(".turnplate-outer").hide();
        $('.turnplate').html('<img id="turnplate-img" src="img/turnplate.png" alt="">');
        $(".one-more-time").show();
        turning = false;
    }, 3000);
});


$(".btn-again").on("click", function(e) {
    $(".one-more-time").hide();
    $(".turnplate-outer").show();
    return false;
});