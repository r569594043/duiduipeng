$(".signup-free").click(function() {
    $("#signin-form").hide();
    $("#signup-form").show();
});

$(".to-signin").click(function() {
    $("#signup-form").hide();
    $("#signin-form").show();
});


$("#signup-form").validate({
    "errorClass": "invalid",
    "rules": {
        "signup-mobile": {
            "required": true,
            "pattern": /^1[3|4|5|7|8]\d{9}$/
        },
        "signup-mobile-captcha": "required",
        "signup-password": {
            "required": true,
            "minlength": 6,
            "maxlength": 16,
            "pattern": /^[a-zA-Z\d~!@#$%^&*()_+{}:"<>?\-=\\\/\.,;'\[\]]{6,16}$/
        },
        "signup-repeat-password": {
            "required": true,
            "equalTo": "#signup-password"
        },
        "signup-city": "required",
        "signup-captcha": "required"
    },
    "messages": {
        "signup-mobile": {
            "required": "手机号码不能为空",
            "pattern": "手机号码格式不正确"
        },
        "signup-mobile-captcha": {
            "required": "验证码不能为空"
        },
        "signup-password": {
            "required": "密码不能为空",
            "minlength": "密码不能少于6位",
            "maxlength": "密码不能多于16位",
            "pattern": "密码只能为6到16位的数字字母或特殊字符"
        },
        "signup-repeat-password": {
            "required": "重复密码不能为空",
            "equalTo": "重复密码不一致"
        },
        "signup-city": {
            "required": "请选择所在城市"
        },
        "signup-captcha": {
            "required": "验证码不能为空"
        }
    },
    "submitHandler": function(form) {
        var $agreeDeal = $("#signup-agree-deal");
        if(!$agreeDeal.is(":checked")) {
            alert("请同意协议");
            return false;
        }
        // 在此实现注册的逻辑，如果通过form表单提交直接将return false 改为 return true;
        alert("验证通过!");
        return false;
    },
    "invalidHandler":function(event, validator) {
        var errorList = validator.errorList;
        if(errorList.length > 0) {
            var first = errorList[0];
            alert(first.message);
            $(first.element).focus();
        }
        return false;
    },
    "errorPlacement": function(error, element) {
        return null;
    }
});

var  rememberedUsername = $.cookie('username');
var rememberedPassword = $.cookie('password');

if(rememberedUsername && rememberedPassword) {
    $("#signin-mobile").val(rememberedUsername);
    $("#signin-password").val(rememberedPassword);
    $("#signin-remember").prop("checked", true);
    $("#signin-remember + label").addClass("checked");
}

$("#signin-form").validate({
    "errorClass": "invalid",
    "rules": {
        "signin-mobile": {
            "required": true,
            "pattern": /^1[3|4|5|7|8]\d{9}$/
        },
        "signin-password": {
            "required": true,
            "minlength": 6,
            "maxlength": 16,
            "pattern": /^[a-zA-Z\d~!@#$%^&*()_+{}:"<>?\-=\\\/\.,;'\[\]]{6,16}$/
        },
        "signin-captcha": "required"
    },
    "messages": {
        "signin-mobile": {
            "required": "手机号码不能为空",
            "pattern": "手机号码格式不正确"
        },
        "signin-password": {
            "required": "密码不能为空",
            "minlength": "密码不能少于6位",
            "maxlength": "密码不能多于16位",
            "pattern": "密码只能为6到16位的数字字母或特殊字符"
        },
        "signin-captcha": {
            "required": "验证码不能为空"
        }
    },
    "submitHandler": function(form) {
        if($("#signin-remember").is(":checked")) {
            $.cookie('username', $("#signin-mobile").val(), { expires: 365 });
            $.cookie('password', $("#signin-password").val(), { expires: 365 });
        }
        // 在此实现登录的逻辑，如果通过form表单提交直接将return false 改为 return true;
        // alert("验证通过!");
        window.location.href="index.html";
        return false;
    },
    "invalidHandler":function(event, validator) {
        var errorList = validator.errorList;
        if(errorList.length > 0) {
            var first = errorList[0];
            alert(first.message);
            $(first.element).focus();
        }
        return false;
    },
    "errorPlacement": function(error, element) {
        return null;
    }
});