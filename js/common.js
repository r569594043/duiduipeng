$.ajaxSetup({
    "cache": false
})

$("select").change(function(e) {
    $(this).blur();
});

$("input:radio + label").click(function() {
    var $this = $(this);
    var $radio = $this.prev();
    var name = $radio.attr("name");
    $('input:radio[name="' + name + '"] + label').removeClass("checked");
    $this.addClass("checked");
});


$("input:checkbox + label").click(function() {
    var $this = $(this);
    $this.toggleClass("checked");
});

var supportPlaceholder = function() {
    var e = document.createElement("input");
    e.type = "text";
    return (typeof e.placeholder !== "undefined");
}();

$(function() {
    if (!supportPlaceholder) {
        var $inputs = $("input:text[placeholder], input:password[placeholder]");
        for (var i = 0; i < $inputs.length; i++) {
            var $input = $inputs.eq(i);
            handlePlaceholder($input);
        }
    }
});
 
function handlePlaceholder($input) {
    var placeholder = $input.attr("placeholder");
    if (placeholder && placeholder.length > 0) {
        if($input.is(':password')) {
            var $placeholder = $('<input type="text" placeholder="' + placeholder + '" style="color:#a9a9a9;" value="' + placeholder + '">');
            $input.after($placeholder).hide();
            $placeholder.focus(function(e) {
                $input.show().focus();
                $placeholder.hide();
            });
            $input.blur(function(e) {
                if ($input.val() === "") {
                    $placeholder.val(placeholder);
                    $placeholder.show();
                    $input.hide();
                }
            })
        } else {
            $input.val(placeholder);
            $input.data("old-color", $input.css("color"));
            $input.css("color", "#a9a9a9");
            $input.focus(function(e) {
                var $this = $(this);
                $this.css("color", $input.data("old-color"));
                if ($this.val() === placeholder) {
                    $this.val("");
                }
            });
            $input.blur(function(e) {
                var $this = $(this);
                if ($this.val() === "") {
                    $this.css("color", "#a9a9a9");
                    $this.val(placeholder);
                }
            });
        }
    }
}

var startsWith = function (str, prefix){
    return str.slice(0, prefix.length) === prefix;
};

var endsWith = function(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
};

if(jQuery.validator) {
    jQuery.validator.addMethod("pattern", function(value, element, param) {
        var pattern, re;
        if(param) {
            if($.type(param) === "regexp") {
                return param.test(value);
            } else if($.type(param) === "string"){
                if(param) {
                    if(!startsWith(param, "^")) {
                        param = "^" + param;
                    }
                    if(!endsWith(param, "$")) {
                        param = param + "$";
                    }
                    re = new RegExp(pattern);
                    return re.test(value);
                }
            }
        }
        var pattern = element.attr("pattern");
        if(pattern) {
            if(!startsWith(pattern, "^")) {
                pattern = "^" + pattern;
            }
            if(!endsWith(pattern, "$")) {
                pattern = pattern + "$";
            }
            re = new RegExp(pattern);
            return re.test(value);
        }
        return true;
    }, "格式不正确");
}